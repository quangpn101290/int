export const APP_BASE_URL = process.env.INTRANET_BASE_URL as string;
export const JWT_ACCESS_TOKEN_SECRET = process.env.JWT_ACCESS_TOKEN_SECRET as string;
