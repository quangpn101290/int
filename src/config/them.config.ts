export const THEMES = {
  LIGHT: 'light',
  DARK: 'dark',
};

export const VERSION_PROJECT = {
  // eslint-disable-next-line @typescript-eslint/no-var-requires
  version: require('../../package.json').version,
};
