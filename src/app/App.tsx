import React from 'react';
import Header from 'src/components/App/Header/Header';
import './App.css';
import { ThemeProvider, createTheme } from '@mui/material/styles';
import Footer from 'src/components/App/Footer';
import GlobalStyles from 'src/styles/globalStyles';
// routes
import Router from '../routes';
import LoadingScreen, { ProgressBarStyle } from 'src/components/App/Loading/LoadingScreen';
import ScrollToTop from 'src/components/App/ScrollToTop/ScrollToTop';
import useAuth from 'src/hooks/useAuth';

const theme = createTheme({
  palette: {},
});

function App() {
  const { isInitialized } = useAuth();

  return (
    <ThemeProvider theme={theme}>
      <GlobalStyles />
      <ProgressBarStyle />
      <ScrollToTop />
      <Header />
      {isInitialized ? <Router /> : <LoadingScreen />}
      <Footer />
    </ThemeProvider>
  );
}

export default App;
