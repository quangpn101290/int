import axios, { AxiosRequestConfig, AxiosResponse } from 'axios';

import { APP_BASE_URL } from 'src/config';
import { getToken } from 'src/utils/handlerCookieClient';

const axiosInstance = axios.create({
  baseURL: APP_BASE_URL,
  withCredentials: true,
  timeout: 30000,
});

axiosInstance.interceptors.request.use(
  async (config: AxiosRequestConfig) => {
    // Do something before request is sent
    const token = await getToken();
    // config.headers.Authorization = `Bearer ${token}`;
    const masterHeaders = {
      'Content-Type': 'application/json;charset=UTF-8',
      Authorization: `Bearer ${token}`,
    };
    config.headers = { ...masterHeaders };

    return config;
  },
  (error) => {
    return Promise.reject(error);
  },
);
axiosInstance.interceptors.response.use(
  (response: AxiosResponse) => {
    const data = response.data;
    if (data.errors && data.errors.length === 0) {
      // In Case: error when empty List.
      delete data.errors;
    }
    return Promise.resolve(response);
  },
  (error) => {
    console.log(error);
    const httpStatus = error.response?.status;
    if (!httpStatus || httpStatus === 500) {
      // Internal Server
    }
    if (httpStatus === 401) {
      // not yet login, redirect to login
    }
    if (httpStatus === 403) {
      // Forbidden
    }
    if (httpStatus === 400) {
      // error when bad request
    }

    return Promise.reject((error.response && error.response.data) || 'Something went wrong');
  },
);
export default axiosInstance;
