import { createContext, useEffect, useReducer } from 'react';
import { AuthAction } from 'src/types/auth/action';
import { addToken, getToken } from 'src/utils/handlerCookieClient';
// utils
import axiosInstance from '../api/axios.interceptor';
import { isValidToken, setSession } from '../utils/jwt';

// ----------------------------------------------------------------------

interface InitializeState {
  isAuthenticated: boolean;
  isInitialized: boolean;
  user: unknown;
}

const initialState = {
  isAuthenticated: false,
  isInitialized: false,
  user: null,
};

const handlers = {
  INITIALIZE: (state: InitializeState, action: AuthAction) => {
    const { isAuthenticated, user } = action.payload;
    return {
      ...state,
      isAuthenticated,
      isInitialized: true,
      user,
    };
  },
  LOGIN: (state: InitializeState, action: AuthAction) => {
    const { user } = action.payload;

    return {
      ...state,
      isAuthenticated: true,
      user,
    };
  },
  LOGOUT: (state: InitializeState) => ({
    ...state,
    isAuthenticated: false,
    user: null,
  }),
  REGISTER: (state: InitializeState, action: AuthAction) => {
    const { user } = action.payload;

    return {
      ...state,
      isAuthenticated: true,
      user,
    };
  },
};

const reducer = (state: InitializeState, action: AuthAction) =>
  handlers[action.type] ? handlers[action.type](state, action) : state;

const AuthContext = createContext({
  ...initialState,
  method: 'jwt',
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  login: (email: string, password: string) => Promise.resolve(),
  logout: () => Promise.resolve(),
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  register: (email: string, password: string, firstName: string, lastName: string) =>
    Promise.resolve(),
});

export interface ProviderProp {
  children: React.ReactNode;
}

export const AuthProvider: React.FC<ProviderProp> = ({ children }) => {
  const [state, dispatch] = useReducer(reducer, initialState);

  useEffect(() => {
    const initialize = async () => {
      try {
        const accessToken = getToken();

        if (accessToken && isValidToken(accessToken)) {
          setSession(accessToken);

          const response = await axiosInstance.get('/intranet/auth/my-account');
          const { user } = response.data;

          dispatch({
            type: 'INITIALIZE',
            payload: {
              isAuthenticated: true,
              user,
            },
          });
        } else {
          dispatch({
            type: 'INITIALIZE',
            payload: {
              isAuthenticated: false,
              user: null,
            },
          });
        }
      } catch (err) {
        console.error(err);
        dispatch({
          type: 'INITIALIZE',
          payload: {
            isAuthenticated: false,
            user: null,
          },
        });
      }
    };

    initialize();
  }, []);

  const login = async (email: string, password: string) => {
    const response = await axiosInstance.post('/intranet/auth/login', {
      email,
      password,
    });
    const { accessToken, user } = response.data;

    setSession(accessToken);
    dispatch({
      type: 'LOGIN',
      payload: {
        user,
      },
    });
  };

  const register = async (email: string, password: string, firstName: string, lastName: string) => {
    const response = await axiosInstance.post('/intranet/auth/register', {
      email,
      password,
      firstName,
      lastName,
    });
    const { accessToken, user } = response.data;

    addToken(accessToken);
    dispatch({
      type: 'REGISTER',
      payload: {
        user,
      },
    });
  };

  const logout = async () => {
    setSession('');
    dispatch({ type: 'LOGOUT' });
  };

  return (
    <AuthContext.Provider
      value={{
        ...state,
        method: 'jwt',
        login,
        logout,
        register,
      }}
    >
      {children}
    </AuthContext.Provider>
  );
};

export { AuthContext };
