import { createTheme } from '@mui/material/styles';
import lightTheme from './lightTheme';
import darkTheme from './darkTheme';

const typeTheme = [lightTheme, darkTheme];

const themes = (type: number) =>
  createTheme({
    ...typeTheme[type],
  });

export default themes;
