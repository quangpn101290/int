import React, { lazy } from 'react';
import { Navigate, useRoutes } from 'react-router-dom';
import { Loadable } from 'src/components/App/Loading/Loadable';
import AuthGuard from 'src/features/guards/AuthGuard';
import GuestGuard from 'src/features/guards/GuestGuard';

export default function Router() {
  return useRoutes([
    // Authentication
    {
      path: 'auth',
      children: [
        {
          path: 'login',
          element: (
            <GuestGuard>
              <Login />
            </GuestGuard>
          ),
        },
        {
          path: 'register',
          element: (
            <GuestGuard>
              <Register />
            </GuestGuard>
          ),
        },
        { path: 'login-unprotected', element: <Login /> },
        { path: 'reset-password', element: '' },
        { path: 'verify', element: '' },
      ],
    },
    // Admin
    {
      path: 'admin',
      children: [
        {
          path: 'upload',
          element: (
            <AuthGuard>
              <Register />
            </AuthGuard>
          ),
        },
        {
          path: 'users',
          element: '',
        },
      ],
    },
    // Publish
    {
      path: '/',
      element: '',
      children: [
        {
          path: 'home',
          element: '',
        },
        {
          path: 'videos',
          element: <Video />,
        },
        {
          path: 'article',
          element: <Article />,
        },
      ],
    },
    { path: '404', element: <Error404Page /> },
    { path: '*', element: <Navigate to="/404" replace /> },
  ]);
}
// IMPORT COMPONENTS

// Authentication
const Login = Loadable(lazy(() => import('src/features/auth/LoginPage/Login')));
const Register = Loadable(lazy(() => import('src/features/auth/RegisterPage/Register')));
const Video = Loadable(lazy(() => import('src/features/videos/VideosPage/Video')));
const Article = Loadable(lazy(() => import('src/features/article/ArticlePage/Article')));
const Error404Page = Loadable(lazy(() => import('src/features/404/404Page')));
