import { BoxProps } from '@mui/material';
import Box from '@mui/material/Box';
import React from 'react';

export const Flex = (props: BoxProps) => {
  return <Box display="flex" boxSizing="border-box" {...props} />;
};
export const CenterFlex = (props: BoxProps) => {
  return (
    <Box
      display="flex"
      boxSizing="border-box"
      justifyContent="center"
      alignItems="center"
      {...props}
    />
  );
};
