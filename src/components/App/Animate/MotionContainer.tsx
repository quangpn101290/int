// material
import { Box } from '@mui/material';
import { motion } from 'framer-motion';
import { varWrapEnter } from './variants/Wrap';
//

// ----------------------------------------------------------------------

type MProps = {
  open: boolean;
  children?: React.ReactNode;
  initial?: string;
  other?: string;
};

const MotionContainer = (props: MProps) => {
  const { open, children, ...other } = props;
  return (
    <Box
      component={motion.div}
      initial={false}
      animate={open ? 'animate' : 'exit'}
      variants={varWrapEnter}
      {...other}
    >
      {children}
    </Box>
  );
};

export default MotionContainer;
