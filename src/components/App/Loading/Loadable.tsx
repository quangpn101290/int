import React, { Suspense } from 'react';
import LoadingScreen from 'src/components/App/Loading/LoadingScreen';

/* eslint-disable react/display-name */
export const Loadable = (Component: React.FC) => (props: JSX.IntrinsicAttributes) => {
  return (
    <Suspense fallback={<LoadingScreen sx={{}} />}>
      <Component {...props} />
    </Suspense>
  );
};
