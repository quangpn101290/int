import * as React from 'react';
// material
import { Box } from '@mui/material';

// ----------------------------------------------------------------------

const Logo = () => {
  return (
    <Box sx={{ width: 40, height: 40 }}>
      <img src="" alt="" />
    </Box>
  );
};
export default Logo;
