import * as React from 'react';
import { NavLink } from 'react-router-dom';
//MUI
import { Typography } from '@mui/material';
import { makeStyles } from '@mui/styles';
import clsx from 'clsx';

const useStyles = makeStyles(() => ({
  link: {
    textDecoration: 'none',
    color: 'white',
    '&:hover': {
      opacity: '0.6',
    },
    marginLeft: '20px',
  },
}));

const Logo = () => {
  const classes = useStyles();
  return (
    <NavLink to="" color="inherit" className={clsx(classes.link)}>
      <Typography variant="h5">Intranet</Typography>
    </NavLink>
  );
};

export default Logo;
