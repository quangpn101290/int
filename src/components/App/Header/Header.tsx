import * as React from 'react';
// material
import { AppBar } from '@mui/material';
import Toolbar from '@mui/material/Toolbar';
import { makeStyles } from '@mui/styles';
import clsx from 'clsx';
// components
import Logo from './Logo';
import Navbar from './Navbar';

// const useStylesBase = makeStyles((theme: Theme) => ({
//   root: {
//     // backgroundColor: '#e06666',
//     backgroundColor: theme.palette.background.paper,
//     // color: 'red',
//     // boxShadow: 'none',
//   },
// }));

const useStyles = makeStyles(() => ({
  root: {
    display: 'flex',
    justifyContent: 'space-around',
    alignItems: 'center',
    backgroundColor: '#e06666',
    fontSize: '18px',
    boxShadow: 'none',
  },
}));

export default function Header() {
  // Order doesn't matter
  //   const classesBase = useStylesBase();
  const classes = useStyles();

  return (
    <AppBar position="static" sx={{ boxShadow: 0, bgcolor: 'transparent' }}>
      <Toolbar color="primary" className={clsx(classes.root)} disableGutters>
        <Logo />
        <Navbar />
      </Toolbar>
    </AppBar>
  );
}
