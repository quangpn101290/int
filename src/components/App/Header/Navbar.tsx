import * as React from 'react';
import { NavLink } from 'react-router-dom';
//MUI
import { Box } from '@mui/system';
import clsx from 'clsx';
import { makeStyles } from '@mui/styles';
import { Typography } from '@mui/material';

const useStyles = makeStyles(() => ({
  root: {
    display: 'flex',
    justifyContent: 'space-around',
    alignItems: 'center',
    flexDirection: 'row',
  },
  link: {
    textDecoration: 'none',
    color: 'white',
    '&:hover': {
      opacity: '0.6',
    },
    marginLeft: '20px',
  },
}));

const Navbar = () => {
  const classes = useStyles();

  return (
    <Box className={clsx(classes.root)}>
      <NavLink to="/home" color="inherit" className={clsx(classes.link)}>
        <Typography>HOME</Typography>
      </NavLink>
      <NavLink to="/videos" color="inherit" className={clsx(classes.link)}>
        <Typography>VIDEO</Typography>
      </NavLink>
      <NavLink to="/article" color="inherit" className={clsx(classes.link)}>
        <Typography>ARTICLE</Typography>
      </NavLink>
      <NavLink to="/auth/login" color="inherit" className={clsx(classes.link)}>
        <Typography>LOGIN</Typography>
      </NavLink>
    </Box>
  );
};

export default Navbar;
