// icon
import HomeOutlinedIcon from '@mui/icons-material/HomeOutlined';
import VideoLibraryOutlinedIcon from '@mui/icons-material/VideoLibraryOutlined';
import ArticleOutlinedIcon from '@mui/icons-material/ArticleOutlined';
import LoginOutlinedIcon from '@mui/icons-material/LoginOutlined';
import { PATH_ARTICLE, PATH_AUTH, PATH_VIDEOS } from 'src/routes/path';
// ----------------------------------------------------------------------

const menuConfig = [
  {
    title: 'Home',
    icon: HomeOutlinedIcon,
    path: '/',
  },
  {
    title: 'Videos',
    icon: VideoLibraryOutlinedIcon,
    path: PATH_VIDEOS.videos,
  },
  {
    title: 'Article',
    icon: ArticleOutlinedIcon,
    path: PATH_ARTICLE.articles,
  },
  {
    title: 'Login',
    icon: LoginOutlinedIcon,
    path: PATH_AUTH.login,
  },
];

export default menuConfig;
