import React from 'react';
import { Link as RouterLink } from 'react-router-dom';

import { Button, Container, Paper, Typography } from '@mui/material';
import { Box, styled } from '@mui/system';
import { motion } from 'framer-motion';
import MotionContainer from 'src/components/App/Animate/MotionContainer';
import { varBounceIn } from 'src/components/App/Animate/bounce/BounceIn';
import PageNotFoundIllustration from 'src/assets/illustration_404';

const RootStyle = styled(Paper)(({ theme }) => ({
  display: 'flex',
  minHeight: '100vh',
  alignItems: 'center',
  paddingTop: theme.spacing(15),
  paddingBottom: theme.spacing(10),
}));

const Error404Page = () => {
  return (
    <RootStyle title="404 Page Not Found">
      <Container>
        <MotionContainer initial="initial" open>
          <Box sx={{ maxWidth: 480, margin: 'auto', textAlign: 'center' }}>
            <motion.div variants={varBounceIn}>
              <Typography variant="h3" paragraph>
                Sorry, page not found!
              </Typography>
            </motion.div>
            <Typography sx={{ color: 'text.secondary' }}>
              Sorry, we couldn’t find the page you’re looking for. Perhaps you’ve mistyped the URL?
              Be sure to check your spelling.
            </Typography>

            <motion.div variants={varBounceIn}>
              <PageNotFoundIllustration sx={{ height: 260, my: { xs: 5, sm: 10 } }} />
            </motion.div>

            <Button
              style={{ backgroundColor: '#e06666' }}
              color="primary"
              to="/"
              size="large"
              variant="contained"
              component={RouterLink}
            >
              Go to Home
            </Button>
          </Box>
        </MotionContainer>
      </Container>
    </RootStyle>
  );
};

export default Error404Page;
