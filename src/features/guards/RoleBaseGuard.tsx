import { Alert, AlertTitle, Container } from '@mui/material';
import React from 'react';

const useCurrentRole = () => {
  // Logic here to get current user role
  return '';
};

type rProps = {
  accessibleRoles: string;
  children: React.ReactElement;
};

const RoleBaseGuard = (props: rProps) => {
  const { accessibleRoles, children } = props;
  const currentRole = useCurrentRole();
  if (!accessibleRoles.includes(currentRole)) {
    return (
      <Container>
        <Alert severity="error">
          <AlertTitle>Permission Denied</AlertTitle>
          You do not have permission to access this page
        </Alert>
      </Container>
    );
  }

  return <>{children}</>;
};

export default RoleBaseGuard;
