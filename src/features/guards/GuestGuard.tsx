import React from 'react';
import { Navigate } from 'react-router-dom';
import useAuth from 'src/hooks/useAuth';
import { PATH_VIDEOS } from 'src/routes/path';

const GuestGuard = ({ children }: { children: React.ReactElement }) => {
  const { isAuthenticated } = useAuth();
  if (isAuthenticated) {
    return <Navigate to={PATH_VIDEOS.root} />;
  }

  return <>{children}</>;
};

export default GuestGuard;
