import jwtDecode, { JwtPayload } from 'jwt-decode';
import React, { useEffect, useState } from 'react';
import { Navigate } from 'react-router-dom';
import { getToken } from 'src/utils/handlerCookieClient';

const PrivateRoute = (props: { path: string }) => {
  const [isAuthenticated, setIsAuthenticated] = useState(false);
  useEffect(() => {
    const token = getToken();
    if (token) {
      const tokenExpiration = jwtDecode<JwtPayload>(token).exp;
      const dateNow = new Date();
      if (tokenExpiration && tokenExpiration < dateNow.getTime() / 1000) {
        setIsAuthenticated(false);
      } else {
        setIsAuthenticated(true);
      }
    } else {
      setIsAuthenticated(false);
    }
  }, [isAuthenticated]);
  return isAuthenticated ? <Navigate replace to={props.path} /> : <Navigate replace to="/login" />;
};

export default PrivateRoute;
