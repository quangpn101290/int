import jwtDecode, { JwtPayload } from 'jwt-decode';
import axiosInstance from '../api/axios.interceptor';
import { removeToken, addToken } from './handlerCookieClient';

// ----------------------------------------------------------------------

const isValidToken = (accessToken: string) => {
  if (!accessToken) {
    return false;
  }

  const decoded = jwtDecode<JwtPayload>(accessToken).exp;
  const dateNow = new Date();

  return decoded && decoded > dateNow.getTime() / 1000;
};

//  const handleTokenExpired = (exp) => {
//   let expiredTimer;

//   window.clearTimeout(expiredTimer);
//   const currentTime = Date.now();
//   const timeLeft = exp * 1000 - currentTime;
//   console.log(timeLeft);
//   expiredTimer = window.setTimeout(() => {
//     console.log('expired');
//     // You can do what ever you want here, like show a notification
//   }, timeLeft);
// };

const setSession = (accessToken: string) => {
  if (accessToken) {
    addToken(accessToken);
    axiosInstance.defaults.headers.common.Authorization = `Bearer ${accessToken}`;
    // This function below will handle when token is expired
    // const { exp } = jwtDecode(accessToken);
    // handleTokenExpired(exp);
  } else {
    removeToken();
    delete axiosInstance.defaults.headers.common.Authorization;
  }
};

export { isValidToken, setSession };
