import cookies from 'js-cookie';

const TOKEN_KEY = 'Authentication';

const getToken = () => {
  return cookies.get(TOKEN_KEY);
};
const removeToken = () => {
  return cookies.remove(TOKEN_KEY);
};
const addToken = (accessToken: string) => {
  return cookies.set(TOKEN_KEY, accessToken);
};

export { getToken, removeToken, addToken };
