export const InitializeAction = 'INITIALIZE';
export interface InitializeAction {
  type: typeof InitializeAction;
  payload?: any;
}

export const LoginAction = 'LOGIN';
export interface LoginAction {
  type: typeof LoginAction;
  payload?: any;
}
export const RegisterAction = 'REGISTER';
export interface RegisterAction {
  type: typeof RegisterAction;
  payload?: any;
}
export const LogoutAction = 'LOGOUT';
export interface LogoutAction {
  type: typeof LogoutAction;
  payload?: any;
}

export type AuthAction = InitializeAction | LoginAction | RegisterAction | LogoutAction;
