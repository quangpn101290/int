export interface InitializeState {
  isAuthenticated: boolean;
  isInitialized: boolean;
  user: unknown;
}
export interface AuthState {
  id: string;
  name: string;
}
